package com.mintos.weatherapi;

import com.mintos.weatherapi.dto.weather.WeatherResponseDto;
import com.mintos.weatherapi.services.CoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class WeatherController {
    private CoreService service;

    @Autowired
    public WeatherController ( CoreService service ) {
        this.service = service;
    }

    @GetMapping(path = "/weather",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WeatherResponseDto> getWeather( HttpServletRequest request) {
        WeatherResponseDto weather = service.retrieveWeatherForecast(request.getRemoteAddr());
        ResponseEntity<WeatherResponseDto> response = new ResponseEntity<>(weather, HttpStatus.OK);

        return response;

    }

    public String getIpAddress (HttpServletRequest request)  {
        return request.getRemoteAddr();
    }

}
