package com.mintos.weatherapi.services;

import com.mintos.weatherapi.dto.weather.ForecastDto;
import com.mintos.weatherapi.exceptions.LocalhostRequestException;
import com.mintos.weatherapi.jpa.*;
import com.mintos.weatherapi.model.Forecast;
import com.mintos.weatherapi.dto.weather.WeatherResponseDto;
import com.mintos.weatherapi.model.Location;
import org.apache.commons.lang3.time.DateUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CoreService {
    private static final Logger log = LoggerFactory.getLogger(CoreService.class);
    ModelMapper modelMapper;
    GeolocationService geolocationService;
    ForecastService forecastService;
    IpAddressRepository ipRepository;
    LocationRepository locationRepository;
    ForecastRepository forecastRepository;
    RequestHistoryRepository historyRepository;

    @Autowired
    public CoreService ( @Qualifier("default-mapper") ModelMapper modelMapper,
                         GeolocationService geolocationService,
                         ForecastService forecastService,
                         LocationRepository locationRepository,
                         IpAddressRepository ipAddressRepository,
                         ForecastRepository forecastRepository,
                         RequestHistoryRepository requestHistoryRepository
                         ) {
        this.modelMapper = modelMapper;
        this.geolocationService = geolocationService;
        this.forecastService = forecastService;
        this.locationRepository = locationRepository;
        this.ipRepository = ipAddressRepository;
        this.forecastRepository = forecastRepository;
        this.historyRepository = requestHistoryRepository;
    }

    @Transactional
    public WeatherResponseDto retrieveWeatherForecast ( String ipAdress) {
        Date rstart = new Date();

        //TODO get app server IP address for location recognition
        if (ipAdress.contentEquals("127.0.0.1") || ipAdress.contentEquals("0:0:0:0:0:0:0:1")) {
            throw new LocalhostRequestException("Can not determine location for localhost (127.0.0.1)");
        }

        JpaLocation location = getLocation(ipAdress);
        Forecast forecast = getForecasts(location);

        WeatherResponseDto response = new WeatherResponseDto();
        response.setCity(location.getCity());
        response.setCountry(location.getCountryName());
        ForecastDto forecastDto = modelMapper.map(forecast, ForecastDto.class);
        response.setForecast(forecastDto);

        saveRequest (rstart, ipAdress, location);

        return response;
    }

    private JpaLocation getLocation ( String ipAddress ) {
        JpaLocation location;
        JpaIpAddress ip = ipRepository.findByIp(ipAddress);
        if (Optional.ofNullable(ip).isPresent()) {
            log.info("Retrieve location from database");
            location = ip.getLocation();
        } else {
            log.info("Retrieve location from external API");
            Location apiLocation = geolocationService.retrieveLocation(ipAddress);
            JpaLocation existingLocation = locationRepository.findByCityAndCountry(apiLocation.getCity(), apiLocation.getCountry());
            if (existingLocation == null) {
                location = modelMapper.map(apiLocation, JpaLocation.class);
                location = locationRepository.save(location);
            } else {
                location = existingLocation;
            }

            JpaIpAddress newIp = new JpaIpAddress(ipAddress);
            newIp.setLocation(location);
            ipRepository.save(newIp);
        }

        return location;
    }

    private Forecast getForecasts ( JpaLocation location ) {
        Forecast forecast = null;

        Date current = new Date();
        Date from = DateUtils.truncate(current, Calendar.HOUR);
        Date to = DateUtils.addHours(current, 1);
        to = DateUtils.truncate(to, Calendar.HOUR);
        List<JpaForecast> forecastList =  forecastRepository.findByLocationAndWeatherDateBetween(location, from, to);

        if (forecastList.size() > 0) {
            log.info("Retrieved forecast from database");
            forecast = modelMapper.map(forecastList.get(forecastList.size() - 1), Forecast.class);
        } else {
            log.info("Retrieved forecast from external API");
            forecast = forecastService.retrieveWeather(location.getCity(), location.getCountry());
            JpaForecast newJpaForecast = modelMapper.map(forecast, JpaForecast.class);
            newJpaForecast.setLocation(location);
            forecastRepository.save(newJpaForecast);
        }

        return forecast;
    }

    private void  saveRequest (Date rstart, String ipAdress, JpaLocation location) {
        JpaRequestHistory item = new JpaRequestHistory();
        item.setIp(ipAdress);
        item.setCity(location.getCity());
        item.setCountry(location.getCountry());
        item.setCountryName(location.getCountryName());
        item.setStart(rstart);
        Date rend = new Date();
        item.setEnd(rend);
        item.setDuration((int) (rend.getTime() - rstart.getTime()) / 1000);
        historyRepository.save(item);

    }
}
