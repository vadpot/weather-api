package com.mintos.weatherapi.services;

import com.mintos.weatherapi.dto.ipapi.IpapiResponseDto;
import com.mintos.weatherapi.exceptions.GeolocationServiceException;
import com.mintos.weatherapi.model.Location;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


@Component
public class GeolocationServiceImpl implements GeolocationService {
    private RestTemplate restTemplate;
    private ModelMapper modelMapper;
    private static final Logger log = LoggerFactory.getLogger(GeolocationServiceImpl.class);

    public GeolocationServiceImpl ( RestTemplate restTemplate, @Qualifier("default-mapper") ModelMapper modelMapper ) {
        this.restTemplate = restTemplate;
        this.modelMapper = modelMapper;
    }

    @Override
    public Location retrieveLocation (String ipAddress) {
        ResponseEntity<IpapiResponseDto> resp = null;
        try {
            //TODO: URL must be in application.properties
            log.info("Request API for location");
            String url = "https://ipapi.co/" + ipAddress +"/json/";
            resp = restTemplate.getForEntity(url, IpapiResponseDto.class);
        } catch (HttpClientErrorException e) {
            //Resposne 400
            log.error(String.format("Geolocation service returned 400 status code with response: %s", e.getResponseBodyAsString()), e);
            throw new GeolocationServiceException("Geolocation can not be determined");
        } catch (HttpServerErrorException e) {
            //Response 500
            log.error(String.format("Geolocation service returned 500 status code with response: %s", e.getResponseBodyAsString()), e);
            throw new GeolocationServiceException("Geolocation can not be determined");
        } catch (Exception e) {
            log.error("Geolocation service can not be called", e);
            throw new RuntimeException("Geolocation is unavailable, try later");
        }

        return  modelMapper.map(resp.getBody(), Location.class);
    }

}
