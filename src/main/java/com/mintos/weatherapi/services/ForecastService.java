package com.mintos.weatherapi.services;

import com.mintos.weatherapi.model.Forecast;

public interface ForecastService {
    Forecast retrieveWeather ( String city, String countryCode ) ;
}
