package com.mintos.weatherapi.services;

import com.mintos.weatherapi.model.Location;

public interface GeolocationService {
    public Location retrieveLocation ( String ipAddress);

}
