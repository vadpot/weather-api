package com.mintos.weatherapi.services;

import com.mintos.weatherapi.dto.openweathermap.CurrentWeatherResponseDto;
import com.mintos.weatherapi.model.Forecast;
import org.flywaydb.core.internal.util.DateUtils;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component
public class ForecastServiceMapper {
    private static final Logger log = LoggerFactory.getLogger(ForecastServiceMapper.class);

    Converter<CurrentWeatherResponseDto, Forecast> apiToCurrentForecast = new AbstractConverter<CurrentWeatherResponseDto, Forecast>() {
        @Override
        protected Forecast convert ( CurrentWeatherResponseDto respDto ) {
            Forecast dayForecast = new Forecast();
            dayForecast.setTemp(respDto.getMain().getTemp());
            dayForecast.setPressure(respDto.getMain().getPressure());
            dayForecast.setHumidity(respDto.getMain().getHumidity());
            dayForecast.setDescription(respDto.getWeather().get(0).getDescription());

            if (Optional.ofNullable(respDto.getWind()).isPresent()) {
                dayForecast.setWindSpeed(respDto.getWind().getSpeed());
                dayForecast.setWindDirection(respDto.getWind().getDeg());
            }  else {
                dayForecast.setWindSpeed(0);
                dayForecast.setWindDirection(0);
            }

            if (Optional.ofNullable(respDto.getRain()).isPresent())
                dayForecast.setRain(respDto.getRain().getOneHour());
            else
                dayForecast.setRain(0);

            if (Optional.ofNullable(respDto.getSnow()).isPresent())
                dayForecast.setSnow(respDto.getSnow().getOneHour());
            else
                dayForecast.setSnow(0);

            return dayForecast;
        }
    };

    @Bean(name="forecast-mapper")
    public ModelMapper modelMapper() {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setSkipNullEnabled(true);

        modelMapper.addConverter(apiToCurrentForecast);
        return modelMapper;
    }

}
