package com.mintos.weatherapi.services;

import com.mintos.weatherapi.dto.openweathermap.CurrentWeatherResponseDto;
import com.mintos.weatherapi.exceptions.ForecastServiceException;
import com.mintos.weatherapi.model.Forecast;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


@Component
public class ForecastServiceImpl implements ForecastService {
    private static final Logger log = LoggerFactory.getLogger(ForecastServiceImpl.class);
    RestTemplate restTemplate;
    ModelMapper modelMapper;

    public ForecastServiceImpl ( RestTemplate restTemplate, @Qualifier("forecast-mapper") ModelMapper modelMapper ) {
        this.restTemplate = restTemplate;
        this.modelMapper = modelMapper;
    }

    @Override
    public Forecast retrieveWeather ( String city, String countryCode ) {
        ResponseEntity<CurrentWeatherResponseDto> resp = null;
        try {
            //TODO: URL, APIKEY must be in application.properties
            log.info("Request API for weather");
            String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&APPID=4c51442c09d8f9410eb6a120fb9751ef&units=metric",
                    city + "," + countryCode);
            resp = restTemplate.getForEntity(url, CurrentWeatherResponseDto.class);

        } catch (HttpClientErrorException e) {
            //Resposne 400
            log.error(String.format("Openweathermap service returned 400 status code with response: %s", e.getResponseBodyAsString()), e);
            throw new ForecastServiceException("Weather is unavailable, try later");
        } catch (HttpServerErrorException e) {
            //Response 500
            log.error(String.format("Openweathermap service returned 500 status code with response: %s", e.getResponseBodyAsString()), e);
            throw new ForecastServiceException("Weather is unavailable, try later");
        } catch (Exception e) {
            log.error("Openweathermap service can not be called", e);
            throw new RuntimeException("Weather is unavailable, try later");
        }

        return modelMapper.map(resp.getBody(), Forecast.class);
    }


}
