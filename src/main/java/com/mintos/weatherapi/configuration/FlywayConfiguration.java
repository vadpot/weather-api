package com.mintos.weatherapi.configuration;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlywayConfiguration {
    private DatasourceConfiguration config;

    public FlywayConfiguration (DatasourceConfiguration dsConfig) {
        this.config = dsConfig;
    }

    @Bean(initMethod = "migrate")
    public Flyway flyway () {
        Flyway flyway = Flyway.configure()
                .dataSource(config.getUrl(),
                        config.getUsername(),
                        config.getPassword())
                .baselineOnMigrate(true)
                .baselineVersion("0")
                .load();

        return flyway;
    }


}
