package com.mintos.weatherapi.dto.ipapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IpapiResponseDto {
    @JsonProperty("org")
    private String org;
    @JsonProperty("asn")
    private String asn;
    @JsonProperty("languages")
    private String languages;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("country_calling_code")
    private String countryCallingCode;
    @JsonProperty("utc_offset")
    private String utcOffset;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("longitude")
    private double longitude;
    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("postal")
    private String postal;
    @JsonProperty("in_eu")
    private boolean inEu;
    @JsonProperty("continent_code")
    private String continentCode;
    @JsonProperty("country_name")
    private String countryName;
    @JsonProperty("country")
    private String country;
    @JsonProperty("region_code")
    private String regionCode;
    @JsonProperty("region")
    private String region;
    @JsonProperty("city")
    private String city;
    @JsonProperty("ip")
    private String ip;

    public IpapiResponseDto ( ) {
    }

    public String getOrg ( ) {
        return org;
    }

    public void setOrg ( String org ) {
        this.org = org;
    }

    public String getAsn ( ) {
        return asn;
    }

    public void setAsn ( String asn ) {
        this.asn = asn;
    }

    public String getLanguages ( ) {
        return languages;
    }

    public void setLanguages ( String languages ) {
        this.languages = languages;
    }

    public String getCurrency ( ) {
        return currency;
    }

    public void setCurrency ( String currency ) {
        this.currency = currency;
    }

    public String getCountryCallingCode ( ) {
        return countryCallingCode;
    }

    public void setCountryCallingCode ( String countryCallingCode ) {
        this.countryCallingCode = countryCallingCode;
    }

    public String getUtcOffset ( ) {
        return utcOffset;
    }

    public void setUtcOffset ( String utcOffset ) {
        this.utcOffset = utcOffset;
    }

    public String getTimezone ( ) {
        return timezone;
    }

    public void setTimezone ( String timezone ) {
        this.timezone = timezone;
    }

    public double getLongitude ( ) {
        return longitude;
    }

    public void setLongitude ( double longitude ) {
        this.longitude = longitude;
    }

    public double getLatitude ( ) {
        return latitude;
    }

    public void setLatitude ( double latitude ) {
        this.latitude = latitude;
    }

    public String getPostal ( ) {
        return postal;
    }

    public void setPostal ( String postal ) {
        this.postal = postal;
    }

    public boolean getInEu ( ) {
        return inEu;
    }

    public void setInEu ( boolean inEu ) {
        this.inEu = inEu;
    }

    public String getContinentCode ( ) {
        return continentCode;
    }

    public void setContinentCode ( String continentCode ) {
        this.continentCode = continentCode;
    }

    public String getCountryName ( ) {
        return countryName;
    }

    public void setCountryName ( String countryName ) {
        this.countryName = countryName;
    }

    public String getCountry ( ) {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }

    public String getRegionCode ( ) {
        return regionCode;
    }

    public void setRegionCode ( String regionCode ) {
        this.regionCode = regionCode;
    }

    public String getRegion ( ) {
        return region;
    }

    public void setRegion ( String region ) {
        this.region = region;
    }

    public String getCity ( ) {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public String getIp ( ) {
        return ip;
    }

    public void setIp ( String ip ) {
        this.ip = ip;
    }
}
