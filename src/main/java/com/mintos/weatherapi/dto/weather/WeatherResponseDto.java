package com.mintos.weatherapi.dto.weather;

import com.mintos.weatherapi.model.Forecast;

import java.util.List;

public class WeatherResponseDto {
    String city;
    String country;
    ForecastDto forecast;

    public WeatherResponseDto ( ) {
    }

    public String getCity ( ) {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public String getCountry ( ) {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }

    public ForecastDto getForecast ( ) {
        return forecast;
    }

    public void setForecast ( ForecastDto forecast ) {
        this.forecast = forecast;
    }
}
