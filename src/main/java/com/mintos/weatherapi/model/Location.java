package com.mintos.weatherapi.model;

public class Location {
    String ip;
    String latitude;
    String longitude;
    String city;
    String country;
    String countryName;

    public Location ( ) {
    }

    public String getIp ( ) {
        return ip;
    }

    public void setIp ( String ip ) {
        this.ip = ip;
    }

    public String getLatitude ( ) {
        return latitude;
    }

    public void setLatitude ( String latitude ) {
        this.latitude = latitude;
    }

    public String getLongitude ( ) {
        return longitude;
    }

    public void setLongitude ( String longitude ) {
        this.longitude = longitude;
    }

    public String getCity ( ) {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public String getCountry ( ) {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }

    public String getCountryName ( ) {
        return countryName;
    }

    public void setCountryName ( String countryName ) {
        this.countryName = countryName;
    }
}
