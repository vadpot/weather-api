package com.mintos.weatherapi.model;


public class Forecast {
    double temp;
    double pressure;
    int humidity;
    double windSpeed;
    int windDirection;
    double rain;
    double snow;
    String description;

    public Forecast ( ) {
    }

    public double getTemp ( ) {
        return temp;
    }

    public void setTemp ( double temp ) {
        this.temp = temp;
    }

    public double getPressure ( ) {
        return pressure;
    }

    public void setPressure ( double pressure ) {
        this.pressure = pressure;
    }

    public int getHumidity ( ) {
        return humidity;
    }

    public void setHumidity ( int humidity ) {
        this.humidity = humidity;
    }

    public double getWindSpeed ( ) {
        return windSpeed;
    }

    public void setWindSpeed ( double windSpeed ) {
        this.windSpeed = windSpeed;
    }

    public int getWindDirection ( ) {
        return windDirection;
    }

    public void setWindDirection ( int windDirection ) {
        this.windDirection = windDirection;
    }

    public double getRain ( ) {
        return rain;
    }

    public void setRain ( double rain ) {
        this.rain = rain;
    }

    public double getSnow ( ) {
        return snow;
    }

    public void setSnow ( double snow ) {
        this.snow = snow;
    }

    public String getDescription ( ) {
        return description;
    }

    public void setDescription ( String description ) {
        this.description = description;
    }
}
