package com.mintos.weatherapi.exceptions;

public class LocalhostRequestException extends RuntimeException {
    public static final int EXCEPTION_CODE = -4;

    public LocalhostRequestException ( ) {
    }

    public LocalhostRequestException ( String message ) {
        super(message);
    }
}
