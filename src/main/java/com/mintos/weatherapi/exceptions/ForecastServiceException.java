package com.mintos.weatherapi.exceptions;

public class ForecastServiceException extends RuntimeException {
    public static final int EXCEPTION_CODE = -2;
    public ForecastServiceException ( ) {
    }

    public ForecastServiceException ( String message ) {
        super(message);
    }
}
