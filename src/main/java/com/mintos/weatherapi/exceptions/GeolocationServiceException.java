package com.mintos.weatherapi.exceptions;

public class GeolocationServiceException extends RuntimeException {
    public static final int EXCEPTION_CODE = -3;
    public GeolocationServiceException ( ) {
    }

    public GeolocationServiceException ( String message ) {
        super(message);
    }
}
