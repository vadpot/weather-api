package com.mintos.weatherapi;

import com.mintos.weatherapi.dto.weather.ErrorResponseDto;
import com.mintos.weatherapi.exceptions.ForecastServiceException;
import com.mintos.weatherapi.exceptions.GeolocationServiceException;
import com.mintos.weatherapi.exceptions.LocalhostRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlingAdvice {

    @ExceptionHandler(value = {GeolocationServiceException.class})
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorResponseDto> handeGeolocationServiceException( final Exception e) {
        return new ResponseEntity<>( new ErrorResponseDto(GeolocationServiceException.EXCEPTION_CODE, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {ForecastServiceException.class})
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorResponseDto> handeForecastServiceException( final Exception e) {
        return new ResponseEntity<>( new ErrorResponseDto(ForecastServiceException.EXCEPTION_CODE, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {LocalhostRequestException.class})
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorResponseDto> handeLocalhostRequestException( final Exception e) {
        return new ResponseEntity<>( new ErrorResponseDto(LocalhostRequestException.EXCEPTION_CODE, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorResponseDto> otherException( final Exception e) {
        return new ResponseEntity<>( new ErrorResponseDto(-1, "Please contact administrator"), HttpStatus.BAD_REQUEST);
    }

}
