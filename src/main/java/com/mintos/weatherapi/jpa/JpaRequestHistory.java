package com.mintos.weatherapi.jpa;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "request_history")
public class JpaRequestHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    @Column(name = "rstart")
    Date start;
    @Column(name = "rend")
    Date end;
    Integer duration;
    @Column(name = "ip_address")
    String ip;
    private String city;
    private String country;
    private String countryName;

    public JpaRequestHistory ( ) {
    }

    public Integer getId ( ) {
        return id;
    }

    public void setId ( Integer id ) {
        this.id = id;
    }

    public Date getStart ( ) {
        return start;
    }

    public void setStart ( Date start ) {
        this.start = start;
    }

    public Date getEnd ( ) {
        return end;
    }

    public void setEnd ( Date end ) {
        this.end = end;
    }

    public Integer getDuration ( ) {
        return duration;
    }

    public void setDuration ( Integer duration ) {
        this.duration = duration;
    }

    public String getIp ( ) {
        return ip;
    }

    public void setIp ( String ip ) {
        this.ip = ip;
    }

    public String getCity ( ) {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public String getCountry ( ) {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }

    public String getCountryName ( ) {
        return countryName;
    }

    public void setCountryName ( String countryName ) {
        this.countryName = countryName;
    }
}
