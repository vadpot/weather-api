package com.mintos.weatherapi.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<JpaLocation, Integer> {
    JpaLocation findByCityAndCountry (String city, String country);
}
