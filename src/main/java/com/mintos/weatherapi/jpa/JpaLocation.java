package com.mintos.weatherapi.jpa;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "locations")
public class JpaLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    private String latitude;
    private String longitude;
    private String city;
    private String country;
    private String countryName;

    public JpaLocation ( ) {
    }

    public Integer getId ( ) {
        return id;
    }

    public void setId ( Integer id ) {
        this.id = id;
    }

    public String getLatitude ( ) {
        return latitude;
    }

    public void setLatitude ( String latitude ) {
        this.latitude = latitude;
    }

    public String getLongitude ( ) {
        return longitude;
    }

    public void setLongitude ( String longitude ) {
        this.longitude = longitude;
    }

    public String getCity ( ) {
        return city;
    }

    public void setCity ( String city ) {
        this.city = city;
    }

    public String getCountry ( ) {
        return country;
    }

    public void setCountry ( String country ) {
        this.country = country;
    }

    public String getCountryName ( ) {
        return countryName;
    }

    public void setCountryName ( String countryName ) {
        this.countryName = countryName;
    }

}
