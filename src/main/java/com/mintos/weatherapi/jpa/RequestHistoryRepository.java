package com.mintos.weatherapi.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestHistoryRepository extends JpaRepository<JpaRequestHistory, Integer> {
}
