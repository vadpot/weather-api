package com.mintos.weatherapi.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IpAddressRepository extends JpaRepository<JpaIpAddress, Integer> {
    JpaIpAddress findByIp( String ipAddress);
}
