package com.mintos.weatherapi.jpa;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "iplocations")
public class JpaIpAddress
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    @Column(name = "ip_address")
    String ip;
    @ManyToOne
    JpaLocation location;

    public JpaIpAddress ( ) {
    }

    public JpaIpAddress ( String ip ) {
        this.ip = ip;
    }

    public Integer getId ( ) {
        return id;
    }

    public void setId ( Integer id ) {
        this.id = id;
    }

    public String getIp ( ) {
        return ip;
    }

    public void setIp ( String ip ) {
        this.ip = ip;
    }

    public JpaLocation getLocation ( ) {
        return location;
    }

    public void setLocation ( JpaLocation location ) {
        this.location = location;
    }

}
