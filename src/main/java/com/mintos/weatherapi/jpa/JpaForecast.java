package com.mintos.weatherapi.jpa;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "forecast")
public class JpaForecast {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    @ManyToOne
    JpaLocation location;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date weatherDate;
    double temp;
    double pressure;
    int humidity;
    double windSpeed;
    int windDirection;
    double rain;
    double snow;
    String description;

    public JpaForecast ( ) {
    }

    public Integer getId ( ) {
        return id;
    }

    public void setId ( Integer id ) {
        this.id = id;
    }

    public JpaLocation getLocation ( ) {
        return location;
    }

    public void setLocation ( JpaLocation location ) {
        this.location = location;
    }

    public Date getWeatherDate ( ) {
        return weatherDate;
    }

    public void setWeatherDate ( Date weatherDate ) {
        this.weatherDate = weatherDate;
    }

    public double getTemp ( ) {
        return temp;
    }

    public void setTemp ( double temp ) {
        this.temp = temp;
    }

    public double getPressure ( ) {
        return pressure;
    }

    public void setPressure ( double pressure ) {
        this.pressure = pressure;
    }

    public int getHumidity ( ) {
        return humidity;
    }

    public void setHumidity ( int humidity ) {
        this.humidity = humidity;
    }

    public double getWindSpeed ( ) {
        return windSpeed;
    }

    public void setWindSpeed ( double windSpeed ) {
        this.windSpeed = windSpeed;
    }

    public int getWindDirection ( ) {
        return windDirection;
    }

    public void setWindDirection ( int windDirection ) {
        this.windDirection = windDirection;
    }

    public double getRain ( ) {
        return rain;
    }

    public void setRain ( double rain ) {
        this.rain = rain;
    }

    public double getSnow ( ) {
        return snow;
    }

    public void setSnow ( double snow ) {
        this.snow = snow;
    }

    public String getDescription ( ) {
        return description;
    }

    public void setDescription ( String description ) {
        this.description = description;
    }
}
