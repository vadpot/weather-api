package com.mintos.weatherapi.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ForecastRepository extends JpaRepository<JpaForecast, Integer> {
    List<JpaForecast> findByLocation( JpaLocation location);
    List<JpaForecast> findByLocationAndWeatherDateBetween( JpaLocation location, Date first, Date second);
}
