CREATE TABLE locations (
   id               SERIAL PRIMARY KEY,
   latitude         VARCHAR,
   longitude        VARCHAR,
   city             VARCHAR,
   country          VARCHAR,
   country_name     VARCHAR
);

CREATE TABLE iplocations (
   id               SERIAL PRIMARY KEY,
   ip_address       VARCHAR,
   location_id   INTEGER
);

CREATE TABLE forecast (
   id               SERIAL PRIMARY KEY,
   location_id      INTEGER,
   weather_date     TIMESTAMP,
   temp             NUMERIC(10, 2),
   pressure         NUMERIC(10, 2),
   humidity         INTEGER,
   wind_speed       NUMERIC(10, 2),
   wind_direction   INTEGER,
   rain             NUMERIC(10, 2),
   snow             NUMERIC(10, 2),
   description      VARCHAR
);