CREATE TABLE request_history (
   id               SERIAL PRIMARY KEY,
   rstart           TIMESTAMP,
   rend             TIMESTAMP,
   duration         INTEGER,
   ip_address       VARCHAR,
   city             VARCHAR,
   country          VARCHAR,
   country_name     VARCHAR
);