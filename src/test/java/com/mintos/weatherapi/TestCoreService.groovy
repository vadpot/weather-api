package com.mintos.weatherapi

import com.fasterxml.jackson.databind.ObjectMapper
import com.mintos.weatherapi.exceptions.ForecastServiceException
import com.mintos.weatherapi.exceptions.GeolocationServiceException
import com.mintos.weatherapi.exceptions.LocalhostRequestException
import com.mintos.weatherapi.jpa.ForecastRepository
import com.mintos.weatherapi.jpa.IpAddressRepository
import com.mintos.weatherapi.jpa.JpaForecast
import com.mintos.weatherapi.jpa.JpaIpAddress
import com.mintos.weatherapi.jpa.JpaLocation
import com.mintos.weatherapi.jpa.LocationRepository
import com.mintos.weatherapi.model.Forecast
import com.mintos.weatherapi.model.Location
import com.mintos.weatherapi.services.ForecastService
import com.mintos.weatherapi.services.GeolocationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.RequestPostProcessor
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@AutoConfigureMockMvc
@SpringBootTest(classes = TestApplication.class)
class TestCoreService extends Specification {
    @Autowired
    private MockMvc mvc
    @Autowired
    private GeolocationService geolocationServiceMock = Stub()
    @Autowired
    private ForecastService forecastServiceMock = Stub()
    @Autowired
    private ForecastRepository forecastRepository
    @Autowired
    private IpAddressRepository ipAddressRepository
    @Autowired
    private LocationRepository locationRepository

    def mapper = new ObjectMapper()
    def location = new Location()
    def forecast = new Forecast()

    def ipAddress = "87.226.88.14"

    def setup () {
        location.setCity("Riga")
        location.setCountry("LV")
        location.setCountryName("Latvia")
        location.setLatitude("1")
        location.setLongitude("1")

        forecast.setDescription("clouds")
        forecast.setHumidity(100)
        forecast.setPressure(1200)
        forecast.setTemp(21)
    }

    def cleanup () {
        forecastRepository.deleteAllInBatch()
        ipAddressRepository.deleteAllInBatch()
        locationRepository.deleteAllInBatch()
    }

    def "Weather service OK, Get geolocation and forecast from external API" () {
        given:
        geolocationServiceMock.retrieveLocation(_) >> location
        forecastServiceMock.retrieveWeather(_, _) >> forecast

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        def ipDB = ipAddressRepository.findByIp(ipAddress)
        def locationDB = new JpaLocation()
        locationDB.setCity(location.getCity())
        locationDB.setCountry(location.getCountry())
        locationDB.setCountryName(location.getCountryName())
        locationDB.setLongitude(location.getLongitude())
        locationDB.setLatitude(location.getLatitude())
        def forecastDB = forecastRepository.findAll()

        then:
        resp.status == HttpStatus.OK.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("city").textValue().equals("Riga")
        responseJson.get("country").textValue().equals("Latvia")
        responseJson.get("forecast").get("humidity").intValue() == forecast.getHumidity()
        responseJson.get("forecast").get("pressure").intValue() == forecast.getPressure()
        responseJson.get("forecast").get("temp").intValue() == forecast.getTemp()

        //check DB persistance
        ipDB.getIp() == ipAddress
        ipDB.getLocation().getCity() == location.getCity()
        ipDB.getLocation().getCountry() == location.getCountry()
        forecastDB.size() == 1
        forecastDB.get(0).getHumidity().intValue() == forecast.getHumidity()
        forecastDB.get(0).getPressure().intValue() == forecast.getPressure()
        forecastDB.get(0).getTemp().intValue() == forecast.getTemp()
    }


    def "Weather service OK, Get geolocation from DB and forecast from external API" () {
        given:
        forecastServiceMock.retrieveWeather(_, _) >> forecast

        def locationDB = new JpaLocation()
        locationDB.setCity(location.getCity())
        locationDB.setCountry(location.getCountry())
        locationDB.setCountryName(location.getCountryName())
        locationDB.setLongitude(location.getLongitude())
        locationDB.setLatitude(location.getLatitude())
        locationDB = locationRepository.save(locationDB)
        def ipDB = new JpaIpAddress()
        ipDB.setLocation(locationDB)
        ipDB.setIp(ipAddress)
        ipAddressRepository.save(ipDB)

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        def forecastDB = forecastRepository.findAll()

        then:
        resp.status == HttpStatus.OK.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("city").textValue().equals(location.getCity())
        responseJson.get("country").textValue().equals(location.getCountryName())
        responseJson.get("forecast").get("humidity").intValue() == forecast.getHumidity()
        responseJson.get("forecast").get("pressure").intValue() == forecast.getPressure()
        responseJson.get("forecast").get("temp").intValue() == forecast.getTemp()

        //check DB persistance
        forecastDB.size() == 1
        forecastDB.get(0).getHumidity().intValue() == forecast.getHumidity()
        forecastDB.get(0).getPressure().intValue() == forecast.getPressure()
        forecastDB.get(0).getTemp().intValue() == forecast.getTemp()
    }

    def "Weather service OK, Get IP from external API for known geolocation and forecast from API" () {
        given:
        geolocationServiceMock.retrieveLocation(_) >> location
        forecastServiceMock.retrieveWeather(_, _) >> forecast

        def locationDB = new JpaLocation()
        locationDB.setCity(location.getCity())
        locationDB.setCountry(location.getCountry())
        locationDB.setCountryName(location.getCountryName())
        locationDB.setLongitude(location.getLongitude())
        locationDB.setLatitude(location.getLatitude())
        locationDB = locationRepository.save(locationDB)

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        def ipDB = ipAddressRepository.findByIp(ipAddress)
        def forecastDB = forecastRepository.findAll()

        then:
        resp.status == HttpStatus.OK.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("city").textValue().equals(location.getCity())
        responseJson.get("country").textValue().equals(location.getCountryName())
        responseJson.get("forecast").get("humidity").intValue() == forecast.getHumidity()
        responseJson.get("forecast").get("pressure").intValue() == forecast.getPressure()
        responseJson.get("forecast").get("temp").intValue() == forecast.getTemp()

        //check DB persistance
        ipDB.getIp() == ipAddress
        ipDB.getLocation().getCity() == location.getCity()
        ipDB.getLocation().getCountry() == location.getCountry()
        forecastDB.size() == 1
        forecastDB.get(0).getHumidity().intValue() == forecast.getHumidity()
        forecastDB.get(0).getPressure().intValue() == forecast.getPressure()
        forecastDB.get(0).getTemp().intValue() == forecast.getTemp()
    }

    def "Weather service OK, Get IP from DB and forecast from DB" () {
        given:
        def locationDB = new JpaLocation()
        locationDB.setCity(location.getCity())
        locationDB.setCountry(location.getCountry())
        locationDB.setCountryName(location.getCountryName())
        locationDB.setLongitude(location.getLongitude())
        locationDB.setLatitude(location.getLatitude())
        locationDB = locationRepository.save(locationDB)
        def ipDB = new JpaIpAddress()
        ipDB.setLocation(locationDB)
        ipDB.setIp(ipAddress)
        ipAddressRepository.save(ipDB)
        def forecastDB = new JpaForecast()
        forecastDB.setHumidity(forecast.getHumidity())
        forecastDB.setPressure(forecast.getPressure())
        forecastDB.setTemp(forecast.getTemp())
        forecastDB.setDescription(forecast.getDescription())
        forecastDB.setLocation(locationDB)
        forecastRepository.save(forecastDB)

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.OK.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("city").textValue().equals(location.getCity())
        responseJson.get("country").textValue().equals(location.getCountryName())
        responseJson.get("forecast").get("humidity").intValue() == forecast.getHumidity()
        responseJson.get("forecast").get("pressure").intValue() == forecast.getPressure()
        responseJson.get("forecast").get("temp").intValue() == forecast.getTemp()
    }

    def "Weather service returns 4XX error when geolocation fails on unparsable response from external API"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >> { throw new RuntimeException("Can not parse JSON response") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue() == -1
    }

    def "Weather service returns 4XX error when geolocation fails on response 4XX from external API"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >>  { throw new GeolocationServiceException("Geolocation service returned 400") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(GeolocationServiceException.EXCEPTION_CODE.intValue())
    }

    def "Weather service returns 4XX error when geolocation fails on response 5XX from external API"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >>  { throw new GeolocationServiceException("Geolocation service returned 500") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(GeolocationServiceException.EXCEPTION_CODE.intValue())
    }

    def "Weather service return 4XX when forecast fails on unparsable json from external API"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >> location
        forecastServiceMock.retrieveWeather(_, _) >> { throw new RuntimeException("Can not parse JSON response") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(-1)
    }

    def "Weather service return 4XX when forecast external API return 4XX"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >> location
        forecastServiceMock.retrieveWeather(_, _) >> { throw new ForecastServiceException("Forecast service returned 400") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(ForecastServiceException.EXCEPTION_CODE.intValue())
    }

    def "Weather service return 4XX when forecast external API return 5XX"() {
        given:
        geolocationServiceMock.retrieveLocation(_) >> location
        forecastServiceMock.retrieveWeather(_, _) >> { throw new ForecastServiceException("Forecast service returned 400") }

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr(ipAddress)))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(ForecastServiceException.EXCEPTION_CODE.intValue())
    }

    def "Weather service is not available if calling from localhost"() {
        given:

        when:
        def resp = mvc.perform(get("/weather")
                .with(remoteAddr("127.0.0.1")))
                .andReturn().response

        then:
        resp.status == HttpStatus.BAD_REQUEST.value()
        def responseJson = mapper.readTree(resp.getContentAsString())
        responseJson.get("code").intValue().equals(LocalhostRequestException.EXCEPTION_CODE.intValue())
    }

    private static RequestPostProcessor remoteAddr(final String remoteAddr) {
        return new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setRemoteAddr(remoteAddr);
                return request;
            }
        };
    }


}