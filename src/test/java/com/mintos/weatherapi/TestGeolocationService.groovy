package com.mintos.weatherapi

import com.mintos.weatherapi.exceptions.GeolocationServiceException
import com.mintos.weatherapi.services.GeolocationServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.web.client.ExpectedCount
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.servlet.request.RequestPostProcessor
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess


@AutoConfigureMockMvc
@SpringBootTest(classes = TestApplication.class)
class TestGeolocationService extends Specification {
    private MockRestServiceServer mockServer
    @Autowired
    @Qualifier("rest-connector")
    private RestTemplate restTemplate
    @Autowired
    private GeolocationServiceImpl service

    def ipAddress = "87.226.88.14"

    def setup () {
        mockServer = MockRestServiceServer.createServer(restTemplate)
    }

    def cleanup () {
        mockServer.reset()
    }


    def "Geolocation service returns correct Location object result"() {
        given:
        def preparedResponse = "{\"ip\": \"87.226.88.14\",\"city\": \"Jelgava\",\"region\": \"Jelgava\",\"region_code\": \"JEL\",\"country\": \"LV\",\"country_name\": \"Latvia\",\"continent_code\": \"EU\",\"in_eu\": true,\"postal\": \"LV-3002\",\"latitude\": 56.65,\"longitude\": 23.7128,\"timezone\": \"Europe/Riga\",\"utc_offset\": \"+0200\",\"country_calling_code\": \"+371\",\"currency\": \"EUR\",\"languages\": \"lv,ru,lt\",\"asn\": \"AS20910\",\"org\": \"SIA Baltcom\"}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://ipapi.co/" + ipAddress +"/json/"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(preparedResponse, MediaType.APPLICATION_JSON))

        when:
        def result = service.retrieveLocation(ipAddress)

        then:
        mockServer.verify()
        result.getCity().equals("Jelgava")
        result.getCountry().equals("LV")
        result.getCountryName().equals("Latvia")
        result.getLatitude().equals("56.65")
        result.getLongitude().equals("23.7128")
    }

    def "Geolocation service fails on unparsable response from external API"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://ipapi.co/" + ipAddress +"/json/"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(preparedResponse, MediaType.APPLICATION_JSON))

        when:
        def result = service.retrieveLocation(ipAddress)

        then:
        mockServer.verify()
        thrown(RuntimeException)
    }

    def "Geolocation service fails on external API responded with 400"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://ipapi.co/" + ipAddress +"/json/"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest())

        when:
        def result = service.retrieveLocation(ipAddress)

        then:
        mockServer.verify()
        thrown(GeolocationServiceException)
    }

    def "Geolocation service fails on external API responded with 500"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://ipapi.co/" + ipAddress +"/json/"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError())

        when:
        def result = service.retrieveLocation(ipAddress)

        then:
        mockServer.verify()
        thrown(GeolocationServiceException)
    }


    private static RequestPostProcessor remoteAddr(final String remoteAddr) {
        return new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setRemoteAddr(remoteAddr);
                return request;
            }
        };
    }
}

