package com.mintos.weatherapi

import com.fasterxml.jackson.databind.ObjectMapper
import com.mintos.weatherapi.exceptions.ForecastServiceException
import com.mintos.weatherapi.model.Location
import com.mintos.weatherapi.services.ForecastService
import com.mintos.weatherapi.services.ForecastServiceImpl
import com.mintos.weatherapi.services.GeolocationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.web.client.ExpectedCount
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.RequestPostProcessor
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static org.hamcrest.core.StringStartsWith.startsWith
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@AutoConfigureMockMvc
@SpringBootTest(classes = TestApplication.class)
class TestForecastService extends Specification {
    private MockRestServiceServer mockServer
    @Autowired
    @Qualifier("rest-connector")
    private RestTemplate restTemplate
    @Autowired
    private ForecastServiceImpl forecastService

    def setup () {
        mockServer = MockRestServiceServer.createServer(restTemplate)
    }

    def cleanup () {
        mockServer.reset()
    }

    def "Forecast service returns correct result"() {
        given:
        def preparedResponse = "{\"coord\":{\"lon\":24.11,\"lat\":56.95},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"base\":\"stations\",\"main\":{\"temp\":-0.96,\"pressure\":1016,\"humidity\":64,\"temp_min\":-1.67,\"temp_max\":0},\"visibility\":10000,\"wind\":{\"speed\":2.6,\"deg\":210},\"clouds\":{\"all\":0},\"dt\":1575196171,\"sys\":{\"type\":1,\"id\":1876,\"country\":\"LV\",\"sunrise\":1575182140,\"sunset\":1575208175},\"timezone\":7200,\"id\":456172,\"name\":\"Riga\",\"cod\":200}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://api.openweathermap.org/data/2.5/weather?q=Riga,LV&APPID=4c51442c09d8f9410eb6a120fb9751ef&units=metric"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(preparedResponse, MediaType.APPLICATION_JSON))

        when:
        def result = forecastService.retrieveWeather("Riga", "LV")

        then:
        mockServer.verify()
        result.description.toString().equals("clear sky")
        result.temp.toDouble() == -0.96
        result.pressure.toInteger() == 1016
        result.humidity.toInteger() == 64
    }

    def "Forecast service fails on unparsable json from external API"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://api.openweathermap.org/data/2.5/weather?q=Riga,LV&APPID=4c51442c09d8f9410eb6a120fb9751ef&units=metric"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(preparedResponse, MediaType.APPLICATION_JSON))

        when:
        def result = forecastService.retrieveWeather("Riga", "LV")

        then:
        mockServer.verify()
        thrown(RuntimeException)
    }

    def "Forecast service fails when external API return 400"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://api.openweathermap.org/data/2.5/weather?q=Riga,LV&APPID=4c51442c09d8f9410eb6a120fb9751ef&units=metric"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withBadRequest())

        when:
        def result = forecastService.retrieveWeather("Riga", "LV")

        then:
        mockServer.verify()
        thrown(ForecastServiceException)
    }

    def "Forecast service fails when external API return 500"() {
        given:
        def preparedResponse = "{not correct json}"

        mockServer.expect(ExpectedCount.once(), requestTo("https://api.openweathermap.org/data/2.5/weather?q=Riga,LV&APPID=4c51442c09d8f9410eb6a120fb9751ef&units=metric"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError())

        when:
        def result = forecastService.retrieveWeather("Riga", "LV")

        then:
        mockServer.verify()
        thrown(ForecastServiceException)
    }

}