package com.mintos.weatherapi

import com.mintos.weatherapi.services.ForecastService
import com.mintos.weatherapi.services.ForecastServiceImpl
import com.mintos.weatherapi.services.GeolocationService
import com.mintos.weatherapi.services.GeolocationServiceImpl
import org.springframework.boot.SpringApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Primary
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.client.RestTemplate
import spock.mock.DetachedMockFactory

import javax.servlet.http.HttpServletRequest

@ComponentScan
@TestConfiguration
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

    @Primary
    @Bean(name = "rest-connector")
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    def factory = new DetachedMockFactory()
    @Primary
    @Bean//(name = "test-geolocation")
    GeolocationService geolocationService() {
        factory.Mock(GeolocationService)
    }

    @Primary
    @Bean//(name = "test-forecast")
    ForecastService forecastServiceMock() {
        factory.Mock(ForecastService)
    }

}